package main

type JsonOVAL struct {
	Advisory []JsonAdvisory `json:"advisory"`
}

type JsonAdvisory struct {
	Title       string       `json:"title"`
	FixesCVE    []string     `json:"fixes_cve"`
	Severity    string       `json:"severity"`
	AffectedCPE []string     `json:"affected_cpe_list"`
	Criteria    JsonCriteria `json:"criteria"`
}

type JsonCriteria struct {
	Operator   string          `json:"operator"`
	Criterias  []*JsonCriteria `json:"criteria"`
	Criterions []JsonCriterion `json:"criterion"`
}

type JsonCriterion struct {
	TestRef string `json:"test_ref"`
	Comment string `json:"comment"`
}

type (
	OvalDefinitions struct {
		Generator      generator       `xml:"generator"`
		Definitions    []definition    `xml:"definitions>definition"`
		RPMInfoTests   []rpmInfoTest   `xml:"tests>rpminfo_test"`
		RPMInfoObjects []rpmInfoObject `xml:"objects>rpminfo_object"`
		RPMInfoStates  []rpmInfoState  `xml:"states>rpminfo_state"`
		/* Other elements defined in the specification we are not using here:
		variables
		ds:Signature
		*/
	}

	generator struct {
		ProductName    string `xml:"product_name"`
		ProductVersion string `xml:"product_version"`
		SchemaVersion  string `xml:"schema_version"`
		Timestamp      string `xml:"timestamp"`
		Any            string `xml:"xsd:any"`
	}

	definition struct {
		Metadata metadata `xml:"metadata"`
		Criteria criteria `xml:"criteria"`
	}

	metadata struct {
		Title       string      `xml:"title"`
		Description string      `xml:"description"`
		References  []reference `xml:"reference"`
		Advisory    advisory    `xml:"advisory"`
		/*Other elements defined in the specification that we are not using here
		affected <- RHEL and SLES have values for this field
		xsd:any
		*/
	}

	advisory struct {
		From     string `xml:"from,attr"`
		Severity string `xml:"severity"`
		Rights   string `xml:"rights"`
		// Issued   date            `xml:"issued"`
		// Updated  date            `xml:"updated"`
		// CVE      cve             `xml:"cve"`
		// Bugzilla bugzilla        `xml:"bugzilla"`
		Affected []affectedCpeList `xml:"affected_cpe_list"`
	}

	affectedCpeList struct {
		Cpe string `xml:"cpe"`
	}

	reference struct {
		Source string `xml:"source,attr"`
		URI    string `xml:"ref_url,attr"`
		RefId  string `xml:"ref_id,attr"`
	}

	criteria struct {
		Operator string `xml:"operator,attr"`
		// Criterias  []*criteria `xml:"criteria"`
		Criterias  []*criteria `xml:"criteria"`
		Criterions []criterion `xml:"criterion"`
		/* Other elements defined in the specification that we are not using here
		applicability_check
		negate
		comment
		extend_definition
		*/
	}

	criterion struct {
		TestRef string `xml:"test_ref,attr"`
		Comment string `xml:"comment,attr"`
		/* Other elements defined in the specification that we are not using here
		applicability_check
		negate
		*/
	}

	rpmInfoTest struct {
		Id      string `xml:"id,attr"`
		Version string `xml:"version,attr"`
		Comment string `xml:"comment,attr"`
		// see http://oval.mitre.org/language/version5.11/ovaldefinition/documentation/oval-common-schema.html#CheckEnumeration
		// Possible values are
		//  all
		//  at least one
		//  none satisfy
		//  only one
		Check     string    `xml:"check,attr"`
		ObjectRef objectRef `xml:"object"`
		StateRef  stateRef  `xml:"state"`
		/* Other elements defined in the specification that we are not using here:
		check_existence
		state_operator
		deprecated
		ds:Signature
		notes
		*/
	}

	objectRef struct {
		Id string `xml:"object_ref,attr"`
	}

	stateRef struct {
		Id string `xml:"state_ref,attr"`
	}

	rpmInfoObject struct {
		Id      string `xml:"id,attr"`
		Version string `xml:"version,attr"`
		Name    string `xml:"name"`
		/* Others elements defined in the specification that we are not using here:
		behaviours
		oval-def:filter
		comment
		deprecated
		*/
	}

	rpmInfoState struct {
		Id      string  `xml:"id,attr"`
		Version string  `xml:"version,attr"`
		Evr     evr     `xml:"evr"`
		Ver     version `xml:"version"`
		/* Other elements defined in the specification that we are not using here:
		name, arch, epoch, release, signature_keyid, extended_name, filepath,
		ds:Signature, notes, operator, comment, deprecated.*/
	}

	evr struct {
		// Expected operations within OVAL for evr_string values are 'equals', 'not equal', 'greater than', 'greater than or equal', 'less than', and 'less than or equal'.
		DataType  string `xml:"datatype,attr"`
		Operation string `xml:"operation,attr"`
		Value     string `xml:",chardata"`
	}

	version struct {
		// Expected operations within OVAL for version values are 'equals', 'not equal', 'greater than', 'greater than or equal', 'less than', and 'less than or equal'.
		// I've seen a "pattern match" in redhat example code which is a OperationEnumeration. I think this should have been in the rpmInfoState>Operator instead
		Operation string `xml:"operation,attr"`
		Value     string `xml:",chardata"`
	}
)
