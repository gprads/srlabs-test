package main

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	resp, err := http.Get("https://www.redhat.com/security/data/oval/com.redhat.rhsa-all.xml")
	if err != nil {
		log.Println(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	// file, err := os.OpenFile("redhat.xml", os.O_WRONLY, 0600)
	// if err != nil {
	// 	log.Println(err)
	// }

	// defer file.Close()

	// file.Write(body)

	var data OvalDefinitions

	err = xml.Unmarshal(body, &data)
	if err != nil {
		log.Println(err)
	}

	var jsonData []JsonAdvisory

	for k := range data.Definitions {

		var m JsonAdvisory
		m.Title = data.Definitions[k].Metadata.Title
		m.Severity = data.Definitions[k].Metadata.Advisory.Severity

		var fixesCPE []string
		for v := range data.Definitions[k].Metadata.References {
			fixesCPE = append(fixesCPE, data.Definitions[k].Metadata.References[v].RefId)
			fixesCPE = fixesCPE[:]
		}
		m.FixesCVE = fixesCPE

		var affectedCpeList []string
		for v := range data.Definitions[k].Metadata.Advisory.Affected {
			affectedCpeList = append(affectedCpeList, data.Definitions[k].Metadata.Advisory.Affected[v].Cpe)
		}
		m.AffectedCPE = affectedCpeList

		m.Criteria.Operator = data.Definitions[k].Criteria.Operator

		// m.Criteria.Criterions = data.Definitions[k].Criteria.Criterions

		// var criterias []*criteria
		// for v := range data.Definitions[k].Criteria.Criterias {
		// 	var m []*criteria
		// 	criterias = append(criterias, *data.Definitions[k].Criteria.Criterias[v])
		// }

		// m.Criteria.Criterias = criterias

		// var criterias []*criteria
		// for v := range data.Definitions[k].Criteria.Criterias {
		// 	criteria = data.Definitions[k].Criteria.
		// }

		// m.Criteria.Criterias = criterias

		// fmt.Println(data.Definitions[k].Metadata.Title)
		jsonData = append(jsonData, m)
	}

	// for k := range data.Definitions {

	// 	// fmt.Println("title :", data.Definitions[k].Metadata.Title)
	// 	// fmt.Println("severity :", data.Definitions[k].Metadata.Advisory.Severity)

	// 	var affectedCpeList []string
	// 	for v := range data.Definitions[k].Metadata.Advisory.Affected {
	// 		affectedCpeList = append(affectedCpeList, data.Definitions[k].Metadata.Advisory.Affected[v].Cpe)
	// 	}
	// 	// fmt.Println("affected_cpe :", affectedCpeList)

	// 	// if len(affectedCpeList) > 1 {
	// 	// 	fmt.Println("affected_cpe :", affectedCpeList)
	// 	// }

	// 	var fixesCPE []string
	// 	for v := range data.Definitions[k].Metadata.References {
	// 		fixesCPE = append(fixesCPE, data.Definitions[k].Metadata.References[v].RefId)
	// 	}
	// 	// fmt.Println("fixes_cpe", fixesCPE)

	// 	// fmt.Println("criteria :", data.Definitions[k].Criteria.Operator)
	// 	// fmt.Println("criterion", data.Definitions[k].Criteria.Criterias)
	// 	var criterias []interface{}
	// 	for v := range data.Definitions[k].Criteria.Criterias {
	// 		criterias = append(criterias, *data.Definitions[k].Criteria.Criterias[v])
	// 	}

	// 	fmt.Println(criterias)

	// 	fmt.Println("-------------------------------")
	// }

	jsonByte, _ := json.Marshal(map[string]interface{}{
		"advisory": jsonData,
	})

	err = ioutil.WriteFile("redhat.json", jsonByte, 0755)
	if err != nil {
		log.Println("Unable to write file: %v", err)
	}

	// file.Write([]byte(jsonData))

	// e := echo.New()
	// e.GET("/json", func(c echo.Context) error {
	// 	return c.JSON(200, echo.Map{
	// 		"Advisory": jsonData,
	// 	})
	// })

	// err = e.Start(":3000")
	// if err != nil {
	// 	panic(err)
	// }
}
